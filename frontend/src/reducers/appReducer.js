// ./react-redux-client/src/reducers/appReducer.js
const INITIAL_STATE = {
  // Here will be initial app state
};

const appReducer = (currentState = INITIAL_STATE, action) => {
  switch (action.type) {

    default:
       return currentState;

  }
};

export default appReducer;
