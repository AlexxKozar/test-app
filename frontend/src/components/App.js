// ./react-redux-client/src/components/App.js
import React from 'react';
import { Navbar } from 'react-bootstrap';
import './App.css';

export default class App extends React.Component {


  render() {
    return (
      <div>
        <Navbar inverse collapseOnSelect className="customNav">
          <Navbar.Header>
            <Navbar.Brand>
              <a href="/#">Test React App is working</a>
            </Navbar.Brand>
            <Navbar.Toggle/>
          </Navbar.Header>
        </Navbar>
      </div>
    );
  }
};
