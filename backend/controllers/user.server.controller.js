// ./express-server/controllers/user.server.controller.js

//import models
import Users from '../models/user.server.model';




export const getUser = (req,res) => {
  console.log(Users);
  Users.find().exec((err,users) => {
    if(err){
      return res.json({'success':false,'message':'Some Error'});
    }

    return res.json({'success':true,'message':'Users fetched successfully', users});
  });
};