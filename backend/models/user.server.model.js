import mongoose from 'mongoose';

const Schema = mongoose.Schema({
  name: String,
  sername: String
});

export default mongoose.model('Users', Schema);
